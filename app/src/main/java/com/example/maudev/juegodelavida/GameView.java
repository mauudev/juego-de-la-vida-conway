package com.example.maudev.juegodelavida;

import android.content.Context;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


public class GameView extends SurfaceView implements SurfaceHolder.Callback {
	

	private GameContext gameContext;
	private boolean gameRunning;
	private Surface surface;
	
	public GameView(Context context) {
		super(context);
		getHolder().addCallback(this);
		setFocusable(true);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {

		if (event.getAction() != MotionEvent.ACTION_UP) {
			gameContext.getEventHandler().addTouch(
					event.getX() / Surface.SCALE,
					event.getY() / Surface.SCALE,
					event.getPressure());
		} else {
			gameContext.getEventHandler().flush();
		}
		
		return true;
	}

	public void setGameContext(GameContext gameContext) {
		this.gameContext = gameContext;
		surface = gameContext.getSurface();
		surface.setSurfaceHolder(getHolder());
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		ThreadTools.debug(this, "Size changed");
		surface.setSize(w, h);
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		ThreadTools.debug(this, "Surface changed");
		surface.setSize(width, height);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		ThreadTools.debug(this, "Surface created");
		
		if (!gameRunning) {
			gameContext.getGameLoop().start();
			gameRunning = true;
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		ThreadTools.debug(this, "Surface destroyed");
	}

}
