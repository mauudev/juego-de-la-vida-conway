package com.example.maudev.juegodelavida;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class SingleReportActivity extends AppCompatActivity {
    private  ArrayList<String> reportes;
    private HashMap<Integer, Integer> genCells;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_report);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView tiempoT = (TextView)findViewById(R.id.tiempoT);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            reportes = extras.getStringArrayList("reportes");
            Intent intent = getIntent();
            genCells = (HashMap<Integer, Integer>) intent.getSerializableExtra("genCells");
            tiempoT.setText(reportes.get(3));
        }
        Button reporteBtn = (Button)findViewById(R.id.reporteBtn1);
        reporteBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (reportes.size() > 0 && genCells.size() > 0) {
                    Intent i = new Intent(getApplicationContext(), SingleReportDataActivity.class);
                    i.putExtra("reportes", reportes);
                    i.putExtra("genCells",genCells);
                    startActivity(i);
                }
            }
        });
        Button regresarBtn = (Button)findViewById(R.id.reset1);
        regresarBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),FormActivity.class);
                startActivity(i);
            }
        });
    }
}
