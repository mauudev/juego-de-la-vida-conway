package com.example.maudev.juegodelavida;


public class GameContext {
	

	private static GameContext instance;
	private ThreadState threadState;
	private final MainThread gameLoop;
	private final Surface surface;
	private final Rules rules;
	private final EventHandler eventHandler;
	private boolean piston = false;


	private GameContext() {
		threadState = ThreadState.RUNNING;
		surface = new Surface(this);
		gameLoop = new MainThread(this);
		rules = new Rules(this);
		eventHandler = new EventHandler();
	}

	public static GameContext getInstance() {
		if (instance == null) {
			instance = new GameContext();
		}
		return instance;
	}

	public void setThreadState(ThreadState threadState) {
		ThreadTools.debug(this, "Setting game threadState to %s", threadState);
		this.threadState = threadState;
	}

	
	public ThreadState getThreadState() {
		return threadState;
	}
	
	public Surface getSurface() {
		return surface;
	}
	
	public MainThread getGameLoop() {
		return gameLoop;
	}

	public Rules getRules() {
		return rules;
	}
	
	public EventHandler getEventHandler() {
		return eventHandler;
	}

	public boolean getPiston(){return this.piston;}
	public void setPiston(boolean piston){this.piston = piston;}
}
