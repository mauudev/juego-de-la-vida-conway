package com.example.maudev.juegodelavida;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class SingleReportDataActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_report_data);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final LinearLayout reportContent = (LinearLayout)findViewById(R.id.reportContent1);
        final TableLayout dataTableResumen = new TableLayout(this);
        final TableLayout dataTableTabla = new TableLayout(this);
        reportContent.removeAllViewsInLayout();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            //***************RESUMEN***************
            ArrayList<String> reportes = extras.getStringArrayList("reportes");
            if(reportes.size() > 0){
                TextView textCellsIni = new TextView(this);
                TextView textCellsFin = new TextView(this);
                TextView textCantGen = new TextView(this);
                TextView textTiempoT = new TextView(this);

                TextView cellsInicio = new TextView(this);
                TextView cellsFin = new TextView(this);
                TextView generaciones = new TextView(this);
                TextView tiempoT = new TextView(this);

                textCellsIni.setText("Cantidad de células al inicio: ");
                textCellsFin.setText("Cantidad de células al final: ");
                textCantGen.setText("Cantidad de generaciones: ");
                textTiempoT.setText("Tiempo transcurrido: ");

                cellsInicio.setText(reportes.get(0)+"");
                cellsFin.setText(reportes.get(1)+"");
                generaciones.setText(reportes.get(2)+"");
                tiempoT.setText(reportes.get(3));

                textCellsIni.setTextSize(20);
                textCellsFin.setTextSize(20);
                textCantGen.setTextSize(20);
                textTiempoT.setTextSize(20);

                cellsInicio.setTextSize(20);
                cellsFin.setTextSize(20);
                generaciones.setTextSize(20);
                tiempoT.setTextSize(20);



                TableRow cellsIniRow = new TableRow(this);
                TableRow cellsFinRow = new TableRow(this);
                TableRow generacionesRow = new TableRow(this);
                TableRow tiempoTRow = new TableRow(this);

                cellsIniRow.addView(textCellsIni);
                cellsIniRow.addView(cellsInicio);

                cellsFinRow.addView(textCellsFin);
                cellsFinRow.addView(cellsFin);

                generacionesRow.addView(textCantGen);
                generacionesRow.addView(generaciones);

                tiempoTRow.addView(textTiempoT);
                tiempoTRow.addView(tiempoT);

                dataTableResumen.addView(cellsIniRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                dataTableResumen.addView(cellsFinRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                dataTableResumen.addView(generacionesRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                dataTableResumen.addView(tiempoTRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
            }
            //***********RESUMEN************

            //***********TABLA**************
            Intent intent = getIntent();
            HashMap<Integer, Integer> genCells = (HashMap<Integer, Integer>) intent.getSerializableExtra("genCells");

            TableRow headerLabelTabla = new TableRow(this);

            TextView labelGen = new TextView(this);
            TextView labelCell = new TextView(this);
            TextView labelNull = new TextView(this);
            labelGen.setText("Gen.");
            labelCell.setText("Cantidad");
            labelGen.setTextSize(18);
            labelCell.setTextSize(18);

            headerLabelTabla.addView(labelGen);
            headerLabelTabla.addView(labelCell);

            dataTableTabla.addView(headerLabelTabla, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));


            if(genCells.size() > 0){
                Map<Integer, Integer> genCellsSorted = new TreeMap<Integer, Integer>(genCells);
                Set set2 = genCellsSorted.entrySet();
                Iterator iterator2 = set2.iterator();
                while(iterator2.hasNext()) {
                    Map.Entry data = (Map.Entry)iterator2.next();
                    int generation = (int)data.getKey();
                    int cantCells = (int)data.getValue();
                    TableRow dataRow = new TableRow(this);
                    TextView dataGenValue = new TextView(this);
                    TextView dataCellsValue = new TextView(this);
                    dataGenValue.setText(generation+"");
                    dataCellsValue.setText(cantCells+"");
                    dataGenValue.setTextSize(14);
                    dataCellsValue.setTextSize(14);
                    dataGenValue.setTextColor(Color.BLUE);
                    dataCellsValue.setTextColor(Color.BLUE);
                    dataRow.addView(dataGenValue);
                    dataRow.addView(dataCellsValue);
                    dataTableTabla.addView(dataRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                }
            }
            GradientDrawable gd=new GradientDrawable();
            gd.setStroke(2, Color.BLACK);
            dataTableTabla.setBackgroundDrawable(gd);
            dataTableTabla.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            //***********TABLA**************
        }

        Button resumenBtn = (Button)findViewById(R.id.resumenBtn1);
        resumenBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                reportContent.removeAllViewsInLayout();
                reportContent.addView(dataTableResumen);
            }
        });

        Button tablaBtn = (Button)findViewById(R.id.tablaBtn1);
        tablaBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                reportContent.removeAllViewsInLayout();
                reportContent.addView(dataTableTabla);
            }
        });

        Button resetBtn = (Button)findViewById(R.id.reset1);
        resetBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                reportContent.removeAllViewsInLayout();
                Intent i = new Intent(getApplicationContext(),FormActivity.class);
                startActivity(i);
            }
        });
    }

}
