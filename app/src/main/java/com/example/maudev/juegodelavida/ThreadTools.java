package com.example.maudev.juegodelavida;

import android.util.Log;

final public class ThreadTools {
	
	private ThreadTools() {
	}

	public static void sleep(long millis) {
		try {
			java.lang.Thread.sleep(millis);
		} catch (InterruptedException e) {
			throw new RuntimeException("Sleep interrupted", e);
		}
	}

	public static void debug(Object source, String message, Object ... args) {
		Log.i(source.getClass().getSimpleName(), String.format(message, args));
	}

}
