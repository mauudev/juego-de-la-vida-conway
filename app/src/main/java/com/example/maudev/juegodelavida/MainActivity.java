package com.example.maudev.juegodelavida;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

//https://code.tutsplus.com/tutorials/android-essentials-creating-simple-user-forms--mobile-1758
public class MainActivity extends Activity  {

    private GameContext gameContext;
    GameView gameView;
    FrameLayout game;
    RelativeLayout GameButtons;
    private final static int id1 = 111;
    private final static int id2 = 222;
    private final static int id3 = 333;
    private final static int id4 = 444;
    private final static int id5 = 555;
    private final static int id6 = 666;
    private ArrayList<Cell> firstCellCollection;
    private Timer timer;
    private ArrayList<String> reportes1 = new ArrayList<String>();
    private HashMap<Integer, Integer> genCells1 = new HashMap<Integer, Integer> ();
    private int[] data;
    @Override
    @SuppressWarnings("ResourceType")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if(extras.getStringArrayList("reportes1") != null){
                reportes1 = extras.getStringArrayList("reportes1");
                System.out.println("reportes: "+reportes1.size());
            }
            Intent intent = getIntent();
            if(intent.getSerializableExtra("genCells1") != null){
                genCells1 = (HashMap<Integer, Integer>) intent.getSerializableExtra("genCells1");
            }
            data = extras.getIntArray("data");
            System.out.println("data: "+data.length);
            System.out.println("reportes: "+reportes1.size());
            System.out.println("hash: "+genCells1.size());
        }

        gameContext = GameContext.getInstance();
        timer = new Timer();
        gameView = new GameView(this);
        game = new FrameLayout(this);
        GameButtons = new RelativeLayout(this);
        Button button1 = new Button(this);
        Button button2 = new Button(this);
        Button button3 = new Button(this);
        //Button button4 = new Button(this);
        Button button5 = new Button(this);
        Button button6 = new Button(this);

        button1.setText("Pausa");
        button2.setText("Reanudar");
        button3.setText("Iniciar");
        //button4.setText("Parar");
        button5.setText("Velocidad +");
        button6.setText("Velocidad -");


        button1.setId(id1);
        button2.setId(id2);
        button3.setId(id3);
        //button4.setId(id4);
        button5.setId(id5);
        button6.setId(id6);

        button1.setTextSize(10);
        button2.setTextSize(10);
        button3.setTextSize(10);
        //button4.setTextSize(10);
        button5.setTextSize(10);
        button6.setTextSize(10);
        //1080 x 1920
        RelativeLayout.LayoutParams b1 = new RelativeLayout.LayoutParams(216,290);//genymotion 100,60
        RelativeLayout.LayoutParams b2 = new RelativeLayout.LayoutParams(216,290);
        RelativeLayout.LayoutParams b3 = new RelativeLayout.LayoutParams(216,290);
        //RelativeLayout.LayoutParams b4 = new RelativeLayout.LayoutParams(216,290);
        RelativeLayout.LayoutParams b5 = new RelativeLayout.LayoutParams(216,290);
        RelativeLayout.LayoutParams b6 = new RelativeLayout.LayoutParams(216,290);
        //RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        //GameButtons.setLayoutParams(params);
        GameButtons.addView(button1);
        GameButtons.addView(button2);
        GameButtons.addView(button3);
        //GameButtons.addView(button4);
        GameButtons.addView(button5);
        GameButtons.addView(button6);

        b1.leftMargin = 0;//10
        b1.topMargin = 1700;//620 geny
        b2.leftMargin = 216;//130
        b2.topMargin = 1700;
        b3.leftMargin = 432;//250
        b3.topMargin = 1700;
//        b4.leftMargin = 525;//370
//        b4.topMargin = 1700;
        b5.leftMargin = 648;//370
        b5.topMargin = 1700;
        b6.leftMargin = 864;//370
        b6.topMargin = 1700;

        button1.setBackgroundColor(Color.YELLOW);//pausa
        button2.setBackgroundColor(Color.CYAN);//reanudar
        button3.setBackgroundColor(Color.GREEN);//iniciar
        //button4.setBackgroundColor(Color.RED);//parar
        button5.setBackgroundColor(Color.MAGENTA);//velocidad +
        button6.setBackgroundColor(Color.MAGENTA);//velocidad -

        button1.setLayoutParams(b1);//pausa
        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(gameContext.getRules().getCells().size() == 0){
                    Toast.makeText(MainActivity.this, "Debe agregar células al tablero !",
                            Toast.LENGTH_LONG).show();
                }else {
                    onPause();
                    timer.pause();
                }
            }
        });
        button2.setLayoutParams(b2);//reanudar
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(gameContext.getRules().getCells().size() == 0){
                    Toast.makeText(MainActivity.this, "Debe agregar células al tablero !",
                            Toast.LENGTH_LONG).show();
                }
                else {
                    onResume();
                    timer.resume();
                }
            }
        });
        button3.setLayoutParams(b3);//iniciar
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(gameContext.getRules().getCells().size() == 0){
                    Toast.makeText(MainActivity.this, "Debe agregar células al tablero !",
                            Toast.LENGTH_LONG).show();
                }
                else{
                    if(reportes1.size() > 0 && genCells1.size() > 0){
                        if(!timer.isRunning()) {
                            timer.reiniciar();
                            new Thread(timer).start();
                            //firstCellCollection.clear();
                            //gameContext.getRules().clearGenerationAndCells();
                            firstCellCollection = (ArrayList)gameContext.getRules().getCells();
                            int cantidadGeneraciones = data[0];
                            gameContext.getRules().setGenerationLim(cantidadGeneraciones);
                            gameContext.setPiston(true);
                            while(true){
                                if(gameContext.getRules().limitGenSuccess()){
                                    gameContext.setPiston(false);
                                    timer.pause();
                                    ArrayList<String> reportes2 = new ArrayList<String>();
                                    HashMap<Integer,Integer> genCells2 = gameContext.getRules().getGenerationAndCells();

                                    reportes2.add(firstCellCollection.size()+"");
                                    reportes2.add(gameContext.getRules().getCells().size()+"");
                                    reportes2.add(gameContext.getRules().tickGeneration()+"");
                                    reportes2.add(timer.getSalida());
                                    System.out.println("DATOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOS");
                                    System.out.println("genC1: "+genCells1.size()+"genC2: "+genCells2.size()+"rep1: "+reportes1.size()+"rep2: "+reportes2.size());
                                    Intent i = new Intent(getApplicationContext(), DoubleReportDataActivity.class);
                                    i.putExtra("genCells1",genCells1);
                                    i.putExtra("reportes1",reportes1);
                                    i.putExtra("genCells2",genCells2);
                                    i.putExtra("reportes2", reportes2);
                                    i.putExtra("data",data);
                                    startActivity(i);
                                    gameContext.getRules().setCellsToNull();
                                    gameContext.getRules().resetGenerations();
                                    timer.reiniciar();
                                    break;
                                }
                            }
                        }
                    }else{
                        if(!timer.isRunning()) {
                            timer.reiniciar();
                            new Thread(timer).start();
                            firstCellCollection = (ArrayList)gameContext.getRules().getCells();
                            int cantidadGeneraciones = data[0];
                            int cantidadSimulaciones = data[1];
                            System.out.println("cantGen: "+cantidadGeneraciones+" cantSim: "+cantidadSimulaciones);
                            gameContext.getRules().setGenerationLim(cantidadGeneraciones);
                            gameContext.setPiston(true);
                            while(true){
                                if(gameContext.getRules().limitGenSuccess()){
                                    gameContext.setPiston(false);
                                    timer.pause();
                                    ArrayList<String> reportes = new ArrayList<String>();
                                    HashMap<Integer,Integer> genCells = gameContext.getRules().getGenerationAndCells();
                                    //System.out.println("SIZE: "+genCells.size());
                                    reportes.add(firstCellCollection.size()+"");
                                    reportes.add(gameContext.getRules().getCells().size()+"");
                                    reportes.add(gameContext.getRules().tickGeneration()+"");
                                    reportes.add(timer.getSalida());
                                    System.out.println("Gen: "+cantidadGeneraciones+" Cant: "+cantidadSimulaciones);
                                    if(cantidadSimulaciones == 1){
                                        Intent i = new Intent(getApplicationContext(), SingleReportActivity.class);
                                        i.putExtra("genCells",genCells);
                                        i.putExtra("reportes", reportes);
                                        startActivity(i);
                                    }else{
                                        Intent i = new Intent(getApplicationContext(), DoubleReportActivity.class);
                                        i.putExtra("genCells1",genCells);
                                        i.putExtra("reportes1", reportes);
                                        i.putExtra("data",data);
                                        startActivity(i);
                                    }
                                    gameContext.getRules().setCellsToNull();
                                    gameContext.getRules().resetGenerations();
                                    timer.reiniciar();
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        });
        //0 -> cantidad cells inicio ; 1 -> cantidad cells final ; 2 -> nro de generaciones
//        button4.setLayoutParams(b4);//parar
//        button4.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                if(gameContext.getRules().getCells().size() > 0) {
//                    gameContext.setPiston(false);
//                    timer.pause();
//                    ArrayList<String> reportes = new ArrayList<String>();
//                    reportes.add(firstCellCollection.size()+"");
//                    reportes.add(gameContext.getRules().getCells().size()+"");
//                    reportes.add(gameContext.getRules().tickGeneration()+"");
//                    reportes.add(timer.getSalida());
//                    Intent i = new Intent(getApplicationContext(), SingleReportActivity.class);
//                    i.putExtra("reportes", reportes);
//                    startActivity(i);
//                    gameContext.getRules().setCellsToNull();
//                    gameContext.getRules().resetGenerations();
//                    timer.reiniciar();
//                }else{
//                    Toast.makeText(MainActivity.this, "Debe agregar células al tablero !",
//                            Toast.LENGTH_LONG).show();
//                }
//            }
//        });

        button5.setLayoutParams(b5);//vel +
        button5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                gameContext.getGameLoop().volumeUp(MainActivity.this);

            }
        });
        button6.setLayoutParams(b6);//vel -
        button6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                gameContext.getGameLoop().volumeDown(MainActivity.this);

            }
        });
        game.addView(gameView);
        game.addView(GameButtons);


        ThreadTools.debug(this, "onCreate()");
        gameView.setGameContext(gameContext);
        setContentView(game);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStop() {
        ThreadTools.debug(this, "onStop()");
        super.onStop();
        gameContext.setThreadState(ThreadState.STOPPED);
    }

    @Override
    protected void onPause() {
        ThreadTools.debug(this, "onPause()");
        super.onPause();
        gameContext.setThreadState(ThreadState.PAUSED);
    }

    @Override
    protected void onResume() {
        //gameContext.setPiston(true);
        ThreadTools.debug(this, "onResume()");
        super.onResume();
        gameContext.setThreadState(ThreadState.RUNNING);
    }
}
