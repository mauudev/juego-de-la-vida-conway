package com.example.maudev.juegodelavida;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

public class MainThread implements Runnable {
	
	private final GameContext context;
	public static long MIN_TICK_TIME = 50;
	private long lastUpdate;
	private java.lang.Thread mainLoop;
	
	public MainThread(GameContext context) {
		this.context = context;
	}


	@Override
	public void run() {
		ThreadTools.debug(this, "Starting game loop");
		while (context.getThreadState() != ThreadState.STOPPED) {
			while (context.getThreadState() == ThreadState.PAUSED) {
				ThreadTools.sleep(100);
			}
			update();
		}
		
		ThreadTools.debug(this, "Stopping game loop");
	}

	public void start() {
		
		if (mainLoop != null) {
			context.setThreadState(ThreadState.RUNNING);
		}
		
		ThreadTools.debug(this, "Starting game loop thread");
		mainLoop = new java.lang.Thread(this);
		mainLoop.start();
	}

	private void update() {
		try {
			context.getRules().tick();
			context.getRules().tickGeneration();
			context.getRules().addGenerationAndCell();
			context.getSurface().update();
			limitFPS();
		} catch (Exception e) {
			Log.e(MainThread.class.getSimpleName(),
					"Unexpected exception in main loop", e);
		}
	}

	private void limitFPS() {
		long now = System.currentTimeMillis();
		if (lastUpdate > 0) {
			long delta = now - lastUpdate;
			if (delta < MIN_TICK_TIME) {
				ThreadTools.sleep(MIN_TICK_TIME - delta);
			}
		}
		lastUpdate = System.currentTimeMillis();
	}

	public void volumeUp(Activity activity){
		if(this.MIN_TICK_TIME >= 50){
			this.MIN_TICK_TIME -= 20;
		}else{
			Toast.makeText(activity, "Velocidad máxima !",
					Toast.LENGTH_LONG).show();
		}
	}

	public void volumeDown(Activity activity){
		if(this.MIN_TICK_TIME <= 200){
			this.MIN_TICK_TIME += 20;
		}else{
			Toast.makeText(activity, "Velocidad mínima !",
					Toast.LENGTH_LONG).show();
		}
	}
}
