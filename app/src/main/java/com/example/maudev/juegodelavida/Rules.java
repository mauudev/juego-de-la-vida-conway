package com.example.maudev.juegodelavida;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Rules {

	private GameContext context;
	private int generations = 0;
	private int limiteGen = 0;
	private volatile Map<Cell, Integer> neighborMap = new HashMap<Cell, Integer>();
	private volatile HashMap<Integer,Integer> generationAndCells = new HashMap<Integer,Integer>();
	private volatile Collection<Cell> cells = new HashSet<Cell>(3000, 0.2f);
	
	public Rules(GameContext context) {
		this.context = context;
	}

	public void tick() {
		Set<Touch> touches = context.getEventHandler().getProcessed();
		for (Touch touch : touches) {
			cells.add(new Cell(touch.x, touch.y));
		}
		//doLogic();
		startLogic(context.getPiston());
	}
	public void startLogic(boolean flag){
		if(flag) doLogic();
	}

	private void doLogic() {
		long start = System.currentTimeMillis();
		Collection<Cell> toRemove = new HashSet<Cell>();
		Collection<Cell> toAdd = new HashSet<Cell>();
		Collection<Cell> newCells = new HashSet<Cell>(cells);
		for (Cell cell : newCells) {
			int neighbors = countNeighbors(cell, newCells);
			
			// Regla 1
			if (neighbors < 2 || neighbors > 3) {
				toRemove.add(cell);
			}
			
			// Rule 2
			if (neighbors > 0) {
				collectNearbyRessurectionCandidates(cell, newCells, toAdd);
			}
		}
		newCells.removeAll(toRemove);
		newCells.addAll(toAdd);
		neighborMap = new HashMap<Cell, Integer>();
		cells = newCells;
		long end = System.currentTimeMillis();
		long delta = end - start;
		if (delta > MainThread.MIN_TICK_TIME) {
			ThreadTools.debug(this, "Game logic took: %s", (end - start));
		}
	}

	private int countNeighbors(Cell cell, Collection<Cell> cells) {
		if (neighborMap.containsKey(cell)) {
			return neighborMap.get(cell);
		}
		int count = 0;
		
		int x = cell.getX();
		int y = cell.getY();
		
		for (int i = x - 1; i <= x + 1; i++) {
			for (int j = y - 1; j <= y + 1; j++) {
				if (isOutOfBounds(i, y)) {
					continue;
				}
				if (i == x && j == y) {
					continue;
				}
				if (cells.contains(new Cell(i, j))) {
					count++;
				}
			}
		}
		neighborMap.put(cell, count);
		return count;
	}

	private void collectNearbyRessurectionCandidates(Cell cell, 
			Collection<Cell> cells,
			Collection<Cell> candidates) {
		
		int x = cell.getX();
		int y = cell.getY();
		for (int i = x - 1; i <= x + 1; i++) {
			for (int j = y - 1; j <= y + 1; j++) {
				if (isOutOfBounds(i, y)) {
					continue;
				}
				if (i == x && j == y) {
					continue;
				}
				Cell c = new Cell(i, j);
				if (cells.contains(c) || candidates.contains(c)) {
					continue;
				} else {
					int neighbours = countNeighbors(c, cells);
					if (neighbours == 3) {
						candidates.add(c);
					}
				}
			}
		}
	}

	private boolean isOutOfBounds(int x, int y) {
		
		if (x < -1 || y < -1 
				|| x > context.getSurface().getMatrixWidth() + 1
				|| y > context.getSurface().getMatrixHeight() + 1) {
			return true;
		}
		
		return false;
	}

	public List<Cell> getCells() {
		return new ArrayList<Cell>(cells);
	}
	public int tickGeneration(){
		if(getCells().size() > 0 && context.getPiston()) this.generations+=1;
		return this.generations;
	}
	public void addGenerationAndCell(){
		generationAndCells.put(getGeneration(),getCells().size());
	}
	public HashMap<Integer,Integer> getGenerationAndCells(){return this.generationAndCells;}
	public int getGeneration(){return this.generations;}
	public boolean limitGenSuccess(){
		return getGeneration() >= getLimiteGen();
	}
	public int getLimiteGen(){return this.limiteGen;}
	public void setGenerationLim(int generationLim){this.limiteGen = generationLim;}
	public void resetGenerations(){this.generations = 0;}
	public GameContext getGameContext(){return this.context;}
	public void setCellsToNull(){
		cells.clear();
	}
	public void clearGenerationAndCells(){generationAndCells.clear();}
}