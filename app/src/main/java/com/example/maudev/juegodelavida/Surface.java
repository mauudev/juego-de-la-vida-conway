package com.example.maudev.juegodelavida;

import java.util.ArrayList;
import java.util.Collection;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.SurfaceHolder;

public class Surface {
	
	private final GameContext context;
	private final Paint bgPaint;
	private final Paint cellPaint;
	private final Paint prePaint;
	private SurfaceHolder surfaceHolder;
	private int width, height;
	
	/**
	 * Escala de un pixel como una celula.
	 */
	//public static final float SCALE = 8f;//genymotion
	public static final float SCALE = 20f;//s4

	public Surface(GameContext context) {
		this.context = context;
		
		bgPaint = new Paint();
		bgPaint.setColor(Color.WHITE);
		
		cellPaint = new Paint();
		cellPaint.setColor(Color.BLACK);
		
		prePaint = new Paint();
		prePaint.setColor(Color.GREEN);
	}

	public void update() {
		Canvas canvas = surfaceHolder.lockCanvas();
		if (canvas != null) {
			prepareBackground(canvas);
			//drawGameCantidadText(canvas,context.getRules().getCells().size());
			//drawGameGenerationsText(canvas,context.getRules().tickGeneration());
			drawCells(canvas);
			drawUnprocessedInput(canvas);
			surfaceHolder.unlockCanvasAndPost(canvas);
		}
	}

	private void prepareBackground(Canvas canvas) {
		//canvas.drawRect(new Rect(0, 0, 480, 620), bgPaint); //para genymotion
		canvas.drawRect(new Rect(0, 0, 1080, 1700), bgPaint);//para s4
	}

	private void drawCells(Canvas canvas) {
		drawCells(canvas, context.getRules().getCells(), cellPaint);
	}

	private void drawGameCantidadText(Canvas canvas,int cantidad){
		CharSequence charSeq = "Cantidad de células: "+ cantidad;
		float x = 10;//10 geny
		float y = 20;//20 geny
		Paint textWColor = new Paint();
		textWColor.setColor(Color.BLACK);
		//textWColor.setTextSize(35);//para s4
		canvas.drawText(charSeq,0,charSeq.length(),x,y,textWColor);
	}

	private void drawGameGenerationsText(Canvas canvas,int cantidad){
		CharSequence charSeq = "Nro. de generaciones: "+ cantidad;
		float x = 10;//10 geny
		float y = 35;//35 geny
		Paint textWColor = new Paint();
		textWColor.setColor(Color.BLACK);
		//textWColor.setTextSize(35);//para s4
		canvas.drawText(charSeq,0,charSeq.length(),x,y,textWColor);
	}

	private void drawUnprocessedInput(Canvas canvas) {
		Collection<Cell> preview = new ArrayList<Cell>();
		for (Touch touch : context.getEventHandler().getUnprocessed()) {
			preview.add(new Cell(touch.x, touch.y));
		}
		drawCells(canvas, preview, prePaint);
	}

	private void drawCells(Canvas canvas, Collection<Cell> cells, Paint paint) {
		for (Cell cell : cells) {
			canvas.drawRect(new Rect(
					Math.round(cell.getX() * SCALE), 
					Math.round(cell.getY() * SCALE), 
					Math.round(cell.getX() * SCALE + SCALE), 
					Math.round(cell.getY() * SCALE + SCALE)), 
					paint);
		}
	}

	public void setSize(int width, int height) {
		ThreadTools.debug(this, "Setting video size: %d x %d", width, height);
		this.width = width;
		this.height = height;
	}

	public int getMatrixWidth() {
		return Math.round(width / SCALE);
	}

	public int getMatrixHeight() {
		return Math.round(height / SCALE);
	}

	public void setSurfaceHolder(SurfaceHolder surfaceHolder) {
		this.surfaceHolder = surfaceHolder;
	}
}
