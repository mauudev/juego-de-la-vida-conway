package com.example.maudev.juegodelavida;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class FormActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_form);
        Button aceptar = (Button)findViewById(R.id.iniciar1);
        final RadioGroup  group = (RadioGroup)findViewById(R.id.radioGroup1);
        final int[] data = new int[2];
        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                View radioButton = radioGroup.findViewById(i);
                RadioButton radioBtn = (RadioButton)findViewById(radioButton.getId());
                TextView cantidadGen = (TextView)findViewById(R.id.numGenVal);
                int cantidadSim = Integer.parseInt(radioBtn.getText().toString());
                int cantidadGeneraciones = Integer.parseInt(cantidadGen.getText().toString());
                data[0] = cantidadGeneraciones;
                data[1] =  cantidadSim;
                System.out.println("Gen: "+cantidadGeneraciones+" Cant: "+cantidadSim);
            }
        });
        aceptar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("data", data);
                startActivity(i);
            }
        });
    }
}
