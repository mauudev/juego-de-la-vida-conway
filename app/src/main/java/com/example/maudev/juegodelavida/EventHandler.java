package com.example.maudev.juegodelavida;

import java.util.HashSet;
import java.util.Set;


public class EventHandler {

	private Set<Touch> unprocessed = new HashSet<Touch>();

	private Set<Touch> processed = new HashSet<Touch>();

	public void addTouch(float x, float y, float pressure) {
		synchronized (unprocessed) {
			unprocessed.add(new Touch(x, y, pressure));
		}
	}
	
	public void flush() {
		synchronized (unprocessed) {
			synchronized (processed) {
				processed = new HashSet<Touch>(unprocessed);
			}
			unprocessed.clear();
		}
	}
	
	public Set<Touch> getUnprocessed() {
		synchronized (unprocessed) {
			return new HashSet<Touch>(unprocessed);
		}
	}

	public Set<Touch> getProcessed() {
		synchronized (processed) {
			Set<Touch> response = new HashSet<Touch>(processed);
			processed.clear();
			return response;
		}
	}
}
