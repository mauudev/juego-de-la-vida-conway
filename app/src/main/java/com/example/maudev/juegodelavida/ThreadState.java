package com.example.maudev.juegodelavida;


public enum ThreadState {
	
	RUNNING, PAUSED, STOPPED
	
}
