package com.example.maudev.juegodelavida;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class DoubleReportActivity extends AppCompatActivity {
    private  ArrayList<String> reportes;
    private HashMap<Integer, Integer> genCells;
    private int[] data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_double_report);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView tiempoT = (TextView)findViewById(R.id.tiempoT);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if(extras.getStringArrayList("reportes1") != null){
                reportes = extras.getStringArrayList("reportes1");
                tiempoT.setText(reportes.get(3));
            }
            Intent intent = getIntent();
            if(intent.getSerializableExtra("genCells1") != null){
                genCells = (HashMap<Integer, Integer>) intent.getSerializableExtra("genCells1");
            }
            data = extras.getIntArray("data");
            System.out.println("data*: "+data.length);
            System.out.println("reportes*: "+reportes.size());
            System.out.println("hash*: "+genCells.size());
        }

        Button continuarBtn = (Button)findViewById(R.id.continuarBtn);
        continuarBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (reportes.size() > 0 && genCells.size() > 0) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    i.putExtra("reportes1", reportes);
                    i.putExtra("genCells1",genCells);
                    i.putExtra("data",data);
                    startActivity(i);
                }
            }
        });
        Button regresarBtn = (Button)findViewById(R.id.reset1);
        regresarBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),FormActivity.class);
                startActivity(i);
            }
        });
    }
}
