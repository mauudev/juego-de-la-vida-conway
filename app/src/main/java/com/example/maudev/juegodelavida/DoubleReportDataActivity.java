package com.example.maudev.juegodelavida;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class DoubleReportDataActivity extends AppCompatActivity {
    private ArrayList<String> reportes1 = new ArrayList<String>();
    private HashMap<Integer, Integer> genCells1 = new HashMap<Integer, Integer> ();
    private ArrayList<String> reportes2 = new ArrayList<String>();
    private HashMap<Integer, Integer> genCells2 = new HashMap<Integer, Integer> ();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_double_report_data);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final LinearLayout reportContent = (LinearLayout)findViewById(R.id.reportContent1);
        final TableLayout dataTableResumen = new TableLayout(this);
        final TableLayout dataTableTabla1 = new TableLayout(this);
        final TableLayout dataTableTabla2 = new TableLayout(this);
        reportContent.removeAllViewsInLayout();
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            //***************RESUMEN***************
            if(extras.getStringArrayList("reportes1") != null){
                reportes1 = extras.getStringArrayList("reportes1");
            }
            Intent intent = getIntent();
            if(intent.getSerializableExtra("genCells1") != null){
                genCells1 = (HashMap<Integer, Integer>) intent.getSerializableExtra("genCells1");
            }
            if(extras.getStringArrayList("reportes2") != null){
                reportes2 = extras.getStringArrayList("reportes2");
            }
            if(intent.getSerializableExtra("genCells2") != null){
                genCells2 = (HashMap<Integer, Integer>) intent.getSerializableExtra("genCells2");
            }

            if(reportes1.size() > 0 && reportes2.size() > 0){
                System.out.println("oli");
                TextView labelSim1 = new TextView(this);
                TextView labelSim2 = new TextView(this);
                TextView textCellsIni1 = new TextView(this);
                TextView textCellsFin1 = new TextView(this);
                TextView textCantGen = new TextView(this);
                TextView textCellsIni2 = new TextView(this);
                TextView textCellsFin2 = new TextView(this);
                TextView textTiempoT = new TextView(this);

                TextView cellsInicio1 = new TextView(this);
                TextView cellsFin1 = new TextView(this);
                TextView cellsInicio2 = new TextView(this);
                TextView cellsFin2 = new TextView(this);
                TextView generaciones = new TextView(this);
                TextView tiempoT = new TextView(this);

                labelSim1.setText("Simulación 1");
                labelSim2.setText("Simulación 2");
                textCellsIni1.setText("Cantidad de células al inicio: ");
                textCellsFin1.setText("Cantidad de células al final: ");
                textCellsIni2.setText("Cantidad de células al inicio: ");
                textCellsFin2.setText("Cantidad de células al final: ");
                textCantGen.setText("Cantidad de generaciones: ");
                textTiempoT.setText("Tiempo transcurrido: ");

                cellsInicio1.setText(reportes1.get(0)+"");
                cellsFin1.setText(reportes1.get(1)+"");
                cellsInicio2.setText(reportes2.get(0)+"");
                cellsFin2.setText(reportes2.get(1)+"");
                generaciones.setText(reportes1.get(2)+"");
                tiempoT.setText(reportes1.get(3));

                labelSim1.setTextColor(Color.BLUE);
                labelSim2.setTextColor(Color.RED);

                labelSim1.setTextSize(25);
                labelSim2.setTextSize(25);
                textCellsIni1.setTextSize(20);
                textCellsFin1.setTextSize(20);
                textCellsIni2.setTextSize(20);
                textCellsFin2.setTextSize(20);
                textCantGen.setTextSize(20);
                textTiempoT.setTextSize(20);

                cellsInicio1.setTextSize(20);
                cellsFin1.setTextSize(20);
                cellsInicio2.setTextSize(20);
                cellsFin2.setTextSize(20);
                generaciones.setTextSize(20);
                tiempoT.setTextSize(20);


                TableRow labelsRow1 = new TableRow(this);
                TableRow cellsIniRow1 = new TableRow(this);
                TableRow cellsFinRow1 = new TableRow(this);
                TableRow generacionesRow = new TableRow(this);
                TableRow tiempoTRow = new TableRow(this);

                TableRow labelsRow2 = new TableRow(this);
                TableRow cellsIniRow2 = new TableRow(this);
                TableRow cellsFinRow2 = new TableRow(this);

                labelsRow1.addView(labelSim1);
                cellsIniRow1.addView(textCellsIni1);
                cellsIniRow1.addView(cellsInicio1);

                cellsFinRow1.addView(textCellsFin1);
                cellsFinRow1.addView(cellsFin1);

                labelsRow2.addView(labelSim2);
                cellsIniRow2.addView(textCellsIni2);
                cellsIniRow2.addView(cellsInicio2);

                cellsFinRow2.addView(textCellsFin2);
                cellsFinRow2.addView(cellsFin2);

                generacionesRow.addView(textCantGen);
                generacionesRow.addView(generaciones);

                tiempoTRow.addView(textTiempoT);
                tiempoTRow.addView(tiempoT);

                dataTableResumen.addView(labelsRow1, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                dataTableResumen.addView(cellsIniRow1, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                dataTableResumen.addView(cellsFinRow1, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                dataTableResumen.addView(labelsRow2, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                dataTableResumen.addView(cellsIniRow2, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                dataTableResumen.addView(cellsFinRow2, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                dataTableResumen.addView(generacionesRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                dataTableResumen.addView(tiempoTRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
            }
            //***********RESUMEN************

            //***********TABLA**************

            TableRow headerLabelTabla1 = new TableRow(this);
            TableRow headerLabelTabla2 = new TableRow(this);

            TextView labelGen1 = new TextView(this);
            TextView labelCell1 = new TextView(this);
            labelGen1.setText("Gen.");
            labelCell1.setText("Cantidad");
            labelGen1.setTextSize(18);
            labelCell1.setTextSize(18);

            TextView labelGen2 = new TextView(this);
            TextView labelCell2 = new TextView(this);
            labelGen2.setText("Gen.");
            labelCell2.setText("Cantidad");
            labelGen2.setTextSize(18);
            labelCell2.setTextSize(18);

            headerLabelTabla1.addView(labelGen1);
            headerLabelTabla1.addView(labelCell1);

            headerLabelTabla2.addView(labelGen2);
            headerLabelTabla2.addView(labelCell2);

            dataTableTabla1.addView(headerLabelTabla1, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
            dataTableTabla2.addView(headerLabelTabla2, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));


            if(genCells1.size() > 0 && genCells2.size() > 0){
                Map<Integer, Integer> genCellsSorted1 = new TreeMap<Integer, Integer>(genCells1);
                Map<Integer, Integer> genCellsSorted2 = new TreeMap<Integer, Integer>(genCells2);
                Set set2 = genCellsSorted1.entrySet();
                Set set3 = genCellsSorted2.entrySet();
                Iterator iterator2 = set2.iterator();
                Iterator iterator3 = set3.iterator();
                while(iterator2.hasNext()) {
                    Map.Entry data = (Map.Entry)iterator2.next();
                    int generation = (int)data.getKey();
                    int cantCells = (int)data.getValue();
                    TableRow dataRow = new TableRow(this);
                    TextView dataGenValue = new TextView(this);
                    TextView dataCellsValue = new TextView(this);
                    dataGenValue.setText(generation+"");
                    dataCellsValue.setText(cantCells+"");
                    dataGenValue.setTextSize(14);
                    dataCellsValue.setTextSize(14);
                    dataGenValue.setTextColor(Color.BLUE);
                    dataCellsValue.setTextColor(Color.BLUE);
                    dataRow.addView(dataGenValue);
                    dataRow.addView(dataCellsValue);
                    dataTableTabla1.addView(dataRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                }
                while(iterator3.hasNext()) {
                    Map.Entry data = (Map.Entry)iterator3.next();
                    int generation = (int)data.getKey();
                    int cantCells = (int)data.getValue();
                    TableRow dataRow = new TableRow(this);
                    TextView dataGenValue = new TextView(this);
                    TextView dataCellsValue = new TextView(this);
                    dataGenValue.setText(generation+"");
                    dataCellsValue.setText(cantCells+"");
                    dataGenValue.setTextSize(14);
                    dataCellsValue.setTextSize(14);
                    dataGenValue.setTextColor(Color.RED);
                    dataCellsValue.setTextColor(Color.RED);
                    dataRow.addView(dataGenValue);
                    dataRow.addView(dataCellsValue);
                    dataTableTabla2.addView(dataRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                }
            }
            GradientDrawable gd1=new GradientDrawable();
            gd1.setStroke(2, Color.BLACK);
            dataTableTabla1.setBackgroundDrawable(gd1);
            dataTableTabla1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            GradientDrawable gd2=new GradientDrawable();
            gd2.setStroke(2, Color.BLACK);
            dataTableTabla2.setBackgroundDrawable(gd2);
            dataTableTabla2.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            //***********TABLA**************
        }

        Button resumenBtn = (Button)findViewById(R.id.resumenBtn1);
        resumenBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                reportContent.removeAllViewsInLayout();
                reportContent.addView(dataTableResumen);
            }
        });

        Button tablaBtn = (Button)findViewById(R.id.tablaBtn1);
        tablaBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                reportContent.removeAllViewsInLayout();
                reportContent.addView(dataTableTabla1);
                reportContent.addView(dataTableTabla2);
            }
        });

        Button resetBtn = (Button)findViewById(R.id.reset1);
        resetBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                reportContent.removeAllViewsInLayout();
                Intent i = new Intent(getApplicationContext(),FormActivity.class);
                startActivity(i);
            }
        });
    }

}
